# ---------------------------------------------------------------------------------------------------------------------
# Project:       crawler_binance
# Description:   Init Flask-app.
# Version:       1.14.11  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from config import FlaskAppConfig


app = Flask(__name__)
app.config.from_object(FlaskAppConfig)
db = SQLAlchemy(app)
