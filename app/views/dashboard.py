# ---------------------------------------------------------------------------------------------------------------------
# Project:       crawler_binance
# Description:   Visualization of data (Dash).
# Version:       1.14.11  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from app.views.routes import app as flask_app
from app.database.db_queries import get_ticker_price, get_ticker_timestamp
from config import dropdown_list, colors


app = dash.Dash(__name__,
                server=flask_app,  # init apps on the same server
                url_base_pathname='/')


app.layout = html.Div(  # view-model
    style={'backgroundColor': colors['background']},
    children=[html.H1(
        children='CRAWLER BINANCE.COM',
        style={
            'textAlign': 'center',
            'color': colors['text']}),
        dcc.Dropdown(
            id='input',
            options=dropdown_list,  # tickers-list
            value=None),  # default value
        html.Div(id='output-graph')])


@app.callback(Output(component_id='output-graph',
                     component_property='children'),
              [Input(component_id='input',
                     component_property='value')])
def update_graph(value):
    """Update data in graphs."""

    return dcc.Graph(id='return-graph',
                     figure={
                         'data': [
                             {'x': get_ticker_timestamp(value),  # time-line
                              'y': get_ticker_price(value),  # price-line
                              'type': 'line', 'name': value}],
                         'layout': {'title': value,
                                    'plot_bgcolor': colors['background'],
                                    'paper_bgcolor': colors['background'],
                                    'font': {'color': colors['text']}}})


if __name__ == '__main__':
    app.run_server(debug=True)
