# ---------------------------------------------------------------------------------------------------------------------
# Project:       crawler_binance
# Description:   API (Flask).
# Version:       1.14.11  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

import json
from flask import request, render_template, session, redirect, url_for

from app.app import app
from app.database.db_queries import (get_user, get_balance, get_ticker,
                                     get_period, get_last)
from app.database.encryptor import encryptor
from app.emulator.trader import trader
from config import tickers


@app.route('/', methods=['GET', 'POST'])
@app.route('/login', methods=['GET', 'POST'])
def login():
    """Check username and password. Open the session."""

    if request.method == 'POST':
        if request.form['username'] == get_user(
                request.form['username']).username and (
                encryptor(request.form['password']) == get_user(
                request.form['username']).password):
            session['logged_in'] = True
            return redirect(url_for('show_balance'))
        else:
            return render_template('login.html',
                                   msg='Invalid username or password.')
    else:
        return render_template('login.html')


@app.route('/logout', methods=['GET'])
def logout():
    """Close the session."""

    session['logged_in'] = False
    return redirect(url_for('login'))


@app.route('/balance', methods=['GET', 'POST'])
def show_balance():
    """Get page with form for request balance."""

    if session.get('logged_in'):
        if request.method == 'POST':
            return render_template('balance.html',
                                   currency=request.form['currency'].upper(),
                                   balance=get_balance(
                                       request.form['currency']).value)

        else:
            return render_template('balance.html')
    else:
        return redirect(url_for('login'))


@app.route('/order', methods=['GET', 'POST'])
def make_order():
    """Get trading emulator page."""

    if session.get('logged_in'):
        if request.method == 'POST':
            return render_template('order.html',
                                   order=trader(request.form['ticker'],
                                                request.form['cur_to_sell'],
                                                request.form['cur_to_buy'],
                                                float(request.form[
                                                          'limit_price']),
                                                float(
                                                    request.form['quantity'])))
        else:
            return render_template('order.html')
    else:
        return redirect(url_for('login'))


@app.route('/api', methods=['GET'])
def api():
    """Get info page -- how to use API."""

    return render_template('api.html')


@app.route('/current/<string:ticker>', methods=['GET'])
def get_current_ticker(ticker):
    """API: Get a specific ticker with the current price."""

    return json.dumps(str(get_ticker(ticker)[-1]))


@app.route('/current', methods=['GET'])
def get_all_current_tickers():
    """API: Get a list of all tickers with the current prices."""

    current_tickers = []
    for ticker in tickers:
        current_tickers.append(get_ticker(ticker)[-1])
    return json.dumps(str(current_tickers))


@app.route('/history/<string:ticker>', methods=['GET'])
def get_ticker_history(ticker):
    """API: Get all history of the ticker."""

    return json.dumps(str(get_ticker(ticker)))


@app.route('/history/<string:ticker>/<date_a>_<date_b>', methods=['GET'])
def get_ticker_history_for_the_period(ticker, date_a, date_b):
    """API: Get a history for the period."""

    return json.dumps(str(get_period(ticker, date_a, date_b)))


@app.route('/history/<string:ticker>/<int:hours>', methods=['GET'])
def get_ticker_history_for_last_hours(ticker, hours):
    """API: Get a history for the period in hours ago."""

    return json.dumps(str(get_last(ticker, hours)))


@app.errorhandler(404)
def page_not_found(error):
    """Handling 404-type errors."""

    return render_template('404.html'), 404


if __name__ == '__main__':
    app.run()
