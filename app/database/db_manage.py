# ---------------------------------------------------------------------------------------------------------------------
# Project:       crawler_binance
# Description:   Database management (PostgreSQL 10).
# Version:       1.14.11  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

from sqlalchemy import (MetaData, Table, ForeignKey, Column, String, Integer,
                        Float, TIMESTAMP)

from app.database.db_models import BalanceTable, UsersTable
from config import (DatabaseConfig, connection_to_default_db, engine_new_db,
                    session, currency, log)


connection_to_default_db.execute('DROP DATABASE IF EXISTS {}'.format(
    DatabaseConfig.new_db))  # delete old database
log.info('[Done] Database "{0}" has been deleted.'.format(
    DatabaseConfig.new_db))

connection_to_default_db.execute('CREATE DATABASE {}'.format(
    DatabaseConfig.new_db))  # create new database
log.info('[Done] Database "{0}" created.'.format(
    DatabaseConfig.new_db))

connection_to_default_db.close()

metadata = MetaData()  # create a model of table
binance_data = Table(DatabaseConfig.table_binance, metadata,
                     Column('id', Integer, primary_key=True),
                     Column('ticker', String, nullable=False),
                     Column('price', Float, nullable=False),
                     Column('timestamp', TIMESTAMP, nullable=False))

users_data = Table(DatabaseConfig.table_users, metadata,
                   Column('id', Integer, primary_key=True),
                   Column('username', String, nullable=False,
                          unique=True),
                   Column('password', String, nullable=False))

balance_data = Table(DatabaseConfig.table_balance, metadata,
                     Column('id', Integer, primary_key=True),
                     Column('currency', String, nullable=False),
                     Column('value', Float, nullable=False),
                     Column('username', String,
                            ForeignKey("{}.username".format(
                                DatabaseConfig.table_users)),
                            nullable=False))

metadata.create_all(engine_new_db)  # create all tables

session.add_all([  # insert start balance and user (test data) in the tables
    UsersTable(username='admin', password='abc'),
    BalanceTable(currency=currency['USDT'], value=99_200, username='admin'),
    BalanceTable(currency=currency['ETH'], value=7_300, username='admin'),
    BalanceTable(currency=currency['BTC'], value=1_500, username='admin')])
session.commit()
log.info('[Done] Tables {0}, {1}, {2} & user created.'
         'Balance updated.'.format(
          DatabaseConfig.table_binance,
          DatabaseConfig.table_users,
          DatabaseConfig.table_balance))
