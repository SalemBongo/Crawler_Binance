# ---------------------------------------------------------------------------------------------------------------------
# Project:       crawler_binance
# Description:   Query-methods for get data from database.
# Version:       1.14.11  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

from datetime import datetime, timedelta

from app.database.db_models import BinanceTable, BalanceTable, UsersTable
from config import session


def get_ticker(ticker: str):
    """Get data for the ticker. Table -- data_binance."""

    return BinanceTable.query.filter_by(ticker=(ticker.upper())).all()


def get_period(ticker: str, date_a: datetime, date_b: datetime):
    """
    Get data for the period. Table -- data_binance.
    date_a: date start.
    date_b: date finish.
    """

    return BinanceTable.query.filter_by(ticker=(ticker.upper())).filter(
        BinanceTable.timestamp.between(date_a, date_b)).all()


def get_last(ticker: str, hours: int):
    """
    Get data for hours ago. Table -- data_binance.
    hours: number of hours ago.
    """

    return BinanceTable.query.filter_by(ticker=(ticker.upper())).filter(
        BinanceTable.timestamp > datetime.now() - timedelta(hours=hours)).\
        filter(BinanceTable.timestamp < datetime.now()).all()


def get_ticker_price(ticker: str):
    """Get timestamp-list of ticker for Dash-app. Table -- data_binance."""

    tickers_data_list = []
    for i in get_ticker(ticker):
        tickers_data_list.append(i.price)
    return tickers_data_list


def get_ticker_timestamp(ticker: str):
    """Get price-list of ticker for Dash-app. Table -- data_binance."""

    tickers_data_list = []
    for i in get_ticker(ticker):
        tickers_data_list.append(i.timestamp)
    return tickers_data_list


def get_balance(currency: str):
    """Get balance for the currency. Table -- trader_balance."""

    return session.query(BalanceTable).filter_by(
        currency=(currency.upper())).first()


def __update_balance(balance_buy: float, balance_sell: float, order: float,
                     cur_to_buy: str, cur_to_sell: str):
    """Update balance. Table -- trader_balance."""

    balance_buy += order
    balance_sell -= order
    session.query(BalanceTable.currency.upper() == cur_to_buy.upper()).\
        filter_by(currency=(cur_to_buy.upper())).update(
        {BalanceTable.value: balance_buy})
    session.query(BalanceTable.currency.upper() == cur_to_sell.upper()).\
        filter_by(currency=(cur_to_sell.upper())).update(
        {BalanceTable.value: balance_sell})


def get_user(username: str):
    """Get data for the user. Table -- users_data."""

    return session.query(UsersTable).filter_by(username=username).first()
