# ---------------------------------------------------------------------------------------------------------------------
# Project:       crawler_binance
# Description:   Models for ORM.
# Version:       1.14.11  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

from app.app import db
from app.database.encryptor import encryptor
from config import DatabaseConfig


class BinanceTable(db.Model):
    """Model of table for data from binance.com."""

    __tablename__ = DatabaseConfig.table_binance

    id = db.Column(db.Integer, primary_key=True)
    ticker = db.Column(db.String, nullable=False)
    price = db.Column(db.Float, nullable=False)
    timestamp = db.Column(db.TIMESTAMP, nullable=False)

    def __init__(self, ticker, price, timestamp):
        self.ticker = ticker
        self.price = price
        self.timestamp = timestamp

    def __repr__(self):
        return '{0}: {1} {2} {3}'.format(
            DatabaseConfig.table_binance,
            self.ticker, self.price, self.timestamp)


class UsersTable(db.Model):
    """Model of table for users data."""

    __tablename__ = DatabaseConfig.table_users

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String, nullable=False, unique=True)
    password = db.Column(db.String, nullable=False)

    balance = db.relationship('BalanceTable',
                              backref=DatabaseConfig.table_users,
                              uselist=False)

    def __init__(self, username, password):
        self.username = username
        self.password = encryptor(password)  # encrypting password

    def __repr__(self):
        return '{0}: {1} {2}'.format(
            DatabaseConfig.table_users, self.password, self.password)


class BalanceTable(db.Model):
    """Model of table for balance data."""

    __tablename__ = DatabaseConfig.table_balance

    id = db.Column(db.Integer, primary_key=True)
    currency = db.Column(db.String, nullable=False)
    value = db.Column(db.Float, nullable=False)
    username = db.Column(db.String, db.ForeignKey("{0}.username".format(
        DatabaseConfig.table_users)), nullable=False)

    user = db.relationship('UsersTable',
                           backref=DatabaseConfig.table_balance,
                           uselist=False)

    def __init__(self, currency, value, username):
        self.currency = currency
        self.value = value
        self.username = username

    def __repr__(self):
        return '{0}: {1} {2}'.format(
            DatabaseConfig.table_balance, self.currency, self.value)
