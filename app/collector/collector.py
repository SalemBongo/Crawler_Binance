# ---------------------------------------------------------------------------------------------------------------------
# Project:       crawler_binance
# Description:   Collection of data (ticker, price) to database.
# Version:       1.14.11  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

import json
import requests
from datetime import datetime

from app.database.db_models import BinanceTable
from config import session, log


def get_binance_data():
    """Get data from binance.com"""

    req = requests.get('https://api.binance.com/api/v3/ticker/price')
    return json.loads(req.text)


def collect(response):
    """Collect data to database with timestamp."""

    try:
        session.add(BinanceTable(ticker=response['symbol'],
                                 price=response['price'],
                                 timestamp=datetime.now()))
        session.commit()
    except Exception as exc:
        log.error(exc)


if __name__ == '__main__':
    """Infinite loop - main scenario."""

    while True:
        collect(get_binance_data()[11])
        collect(get_binance_data()[12])
        collect(get_binance_data()[0])
